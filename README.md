# TypeScript Project Template

This is a typescript template that includes: Eslint setup for typescript, jest for testing 0x for performance profiling, ts-results for better control flow, and codemetrics for complexity tracking

## Collaborate with your team

- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)


## Installation
Run ```npm install``` 


## Authors and acknowledgment
Deveyus

## License
This project is licensed under the MIT license. See LICENSE.md for details.
